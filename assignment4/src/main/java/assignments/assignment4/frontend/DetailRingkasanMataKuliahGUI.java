package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, Mahasiswa[] mahasiswas){
        Container container = frame.getContentPane();
        // TODO: Implementasikan Detail Ringkasan Mata Kuliah
        String nama = "Nama Mata Kuliah: " + mataKuliah.getNama();
        String kode = "Kode: " + mataKuliah.getKode();
        String sks = "SKS: " + mataKuliah.getSKS();
        String jumlah = "Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa();
        String kapasitas = "Kapsitas: " + mataKuliah.getKapasitas();
        String daftar = "Daftar Mahasiswa: ";

        JLabel[] space = new JLabel[100];
        for(int i = 0; i < space.length; i++){
            space[i] = new JLabel(" ");
        }

        JLabel titlLabel = new JLabel("Detail RIngkasan Mata Kuliah"); titlLabel.setFont(SistemAkademikGUI.fontTitle); titlLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel[] aLabels = {new JLabel(nama), new JLabel(kode), new JLabel(sks), new JLabel(jumlah), new JLabel(kapasitas), new JLabel(daftar)};

        container.add(titlLabel);
        container.add(space[0]);

        for(int i = 0; i < aLabels.length; i++){
            aLabels[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            aLabels[i].setFont(SistemAkademikGUI.fontGeneral);
            container.add(aLabels[i]);
            container.add(space[i+1]);
        }

        for(int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++){
            int temp = i+1;
            JLabel tempj = new JLabel(temp + ". " + mahasiswas[i].getNama());
            tempj.setAlignmentX(Component.CENTER_ALIGNMENT);
            tempj.setFont(SistemAkademikGUI.fontGeneral);
            container.add(tempj);
            container.add(space[aLabels.length + i]);
        }

        
        
    }
}
