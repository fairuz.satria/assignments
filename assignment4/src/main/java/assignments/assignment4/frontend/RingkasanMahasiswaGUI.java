package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    private JComboBox acComboBoxs;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        Mahasiswa[] tempmMahasiswas = new Mahasiswa[SistemAkademikGUI.getDaftarMahasiswa().size()];
        SistemAkademikGUI.getDaftarMahasiswa().toArray(tempmMahasiswas);
        acComboBoxs = new JComboBox<>(tempmMahasiswas);
        
        Container container = frame.getContentPane();
        JLabel titLabel = new JLabel("Ringkasan Mahasiswa");
        titLabel.setFont(SistemAkademikGUI.fontTitle);
        titLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        JLabel[] space = new JLabel[11];
        for(int i = 0; i < 11; i++){
            space[i] = new JLabel(" ");
        }

        container.add(titLabel);
        container.add(space[0]);

        JLabel aLabels =new JLabel("Pilih NPM");
        aLabels.setFont(SistemAkademikGUI.fontGeneral);
        aLabels.setAlignmentX(Component.CENTER_ALIGNMENT);
        acComboBoxs.setMaximumSize(new Dimension(200, 24));
        acComboBoxs.setAlignmentX(Component.CENTER_ALIGNMENT);
        container.add(aLabels);
        container.add(space[1]);
        container.add(acComboBoxs);
        container.add(space[2]);

        JButton kembali = new JButton("Kembali");
        JButton lihat = new JButton("Lihat");
        lihat.setFont(SistemAkademikGUI.fontGeneral);
        lihat.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);

        kembali.addActionListener(new Actions(frame, "home"));
        lihat.addActionListener(new lihatRingMahasiswa(frame));

        container.add(lihat);
        container.add(space[5]);
        container.add(kembali);
    }


    // Uncomment method di bawah jika diperlukan

    Mahasiswa getMahasiswa() {
        return (Mahasiswa) acComboBoxs.getSelectedItem();
    }

    class lihatRingMahasiswa implements ActionListener{

        JFrame frame;
    
        public lihatRingMahasiswa(JFrame frame){
            this.frame = frame;
        }
    
        public void actionPerformed(ActionEvent e){
            if(getMahasiswa() == null){
                JOptionPane.showMessageDialog(this.frame, "Mohon isi seluruh Field");
            }else{
               frame.getContentPane().removeAll();
               new DetailRingkasanMahasiswaGUI(frame, getMahasiswa(), SistemAkademikGUI.getDaftarMahasiswa(), getMahasiswa().getMataKuliah());
               frame.invalidate();
               frame.validate();
               frame.repaint();
            }
        }
    }
}




