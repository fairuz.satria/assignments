package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{
    private JTextField namafField = new JTextField();                                                                    // menambahkan object Text Field bawaan swing
    private JTextField nPMField = new JTextField();

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        Container container = frame.getContentPane();

        JButton kembali = new JButton("Kembali");
        JButton tambahkan = new JButton("Tambahkan");

        JLabel titLabel = new JLabel("Tambah Mahasiswa");
        JLabel namalJLabel = new JLabel("Nama:");
        JLabel npmjJLabel = new JLabel("NPM:");

        titLabel.setFont(SistemAkademikGUI.fontTitle);
        titLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        namalJLabel.setFont(SistemAkademikGUI.fontGeneral);
        npmjJLabel.setFont(SistemAkademikGUI.fontGeneral);
        namalJLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        npmjJLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        namafField.setAlignmentX(Component.CENTER_ALIGNMENT);                                                            
        nPMField.setAlignmentX(Component.CENTER_ALIGNMENT);
        namafField.setMaximumSize(new Dimension(200, 24));
        nPMField.setMaximumSize(new Dimension(200, 24));

        tambahkan.setFont(SistemAkademikGUI.fontGeneral);
        tambahkan.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);

        kembali.addActionListener(new Actions(frame, "home"));
        tambahkan.addActionListener(new tambah(frame));
        
        JLabel[] space = new JLabel[7];
        for(int i = 0; i < 7; i++){
            space[i] = new JLabel(" ");
        }


        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.add(titLabel);
        container.add(space[0]);
        container.add(namalJLabel);
        container.add(space[1]);
        container.add(namafField);
        container.add(space[2]);
        container.add(npmjJLabel);
        container.add(space[3]);
        container.add(nPMField);
        container.add(space[4]);
        container.add(tambahkan);
        container.add(space[5]);
        container.add(kembali);
    }

    class tambah implements ActionListener{

        JFrame frame;
    
        public tambah(JFrame frame){
            this.frame = frame;
        }
        public void actionPerformed(ActionEvent e){
            if(namafField.getText().equals("") || nPMField.getText().equals("")){                                        // input dari field diperiksa apakah kosong atau isi
                JOptionPane.showMessageDialog(this.frame, "Mohon isi seluruh Field");                                    // menampilkan message window dengan text sesuai input
            }else{
                long npm = Long.parseLong(nPMField.getText());
                if(already()){                                                                                           // jika npm sudah ada
                    JOptionPane.showMessageDialog(this.frame, "NPM " + nPMField.getText() + " sudah pernah ditambahkan");
                    nPMField.setText("");
                    namafField.setText("");
                }else{
                    SistemAkademikGUI.addDaftarMahasiswa(new Mahasiswa(namafField.getText(), npm));
                    SistemAkademikGUI.sortmhs(SistemAkademikGUI.getDaftarMahasiswa());                                   // sort
                    JOptionPane.showMessageDialog(this.frame, "Mahasiswa " + nPMField.getText() + "-" + namafField.getText() + " berhasil ditambahkan");
                    nPMField.setText("");
                    namafField.setText("");
                }
            }
        }
    
        private boolean already(){
            long npm = Long.parseLong(nPMField.getText());
            for(Mahasiswa x: SistemAkademikGUI.getDaftarMahasiswa()){
                if(npm == x.getNpm()){
                    return true;
                }
            }
            return false;
        }
    }

}
    
