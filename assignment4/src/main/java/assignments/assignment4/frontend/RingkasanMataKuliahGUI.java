package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    private JComboBox acComboBoxs;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        MataKuliah[] tempMatakuliah = new MataKuliah[SistemAkademikGUI.getDaftarMataKuliah().size()];
        SistemAkademikGUI.getDaftarMataKuliah().toArray(tempMatakuliah);
        acComboBoxs = new JComboBox<>(tempMatakuliah);
        
        Container container = frame.getContentPane();
        JLabel titLabel = new JLabel("Ringkasan Mata Kuliah");
        titLabel.setFont(SistemAkademikGUI.fontTitle);
        titLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        JLabel[] space = new JLabel[11];
        for(int i = 0; i < 11; i++){
            space[i] = new JLabel(" ");
        }

        container.add(titLabel);
        container.add(space[0]);

        JLabel aLabels =new JLabel("Pilih Nama MaKul");
        aLabels.setFont(SistemAkademikGUI.fontGeneral);
        aLabels.setAlignmentX(Component.CENTER_ALIGNMENT);
        acComboBoxs.setMaximumSize(new Dimension(200, 24));
        acComboBoxs.setAlignmentX(Component.CENTER_ALIGNMENT);
        container.add(aLabels);
        container.add(space[1]);
        container.add(acComboBoxs);
        container.add(space[2]);

        JButton kembali = new JButton("Kembali");
        JButton lihat = new JButton("Lihat");
        lihat.setFont(SistemAkademikGUI.fontGeneral);
        lihat.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);

        kembali.addActionListener(new Actions(frame, "home"));
        lihat.addActionListener(new lihatRingMatakuliah(frame));

        container.add(lihat);
        container.add(space[5]);
        container.add(kembali);
    }


    // Uncomment method di bawah jika diperlukan

    MataKuliah getMatakuliah() {
        return (MataKuliah) acComboBoxs.getSelectedItem();
    }

    class lihatRingMatakuliah implements ActionListener{

        JFrame frame;
    
        public lihatRingMatakuliah(JFrame frame){
            this.frame = frame;
        }
    
        public void actionPerformed(ActionEvent e){
            if(getMatakuliah() == null){
                JOptionPane.showMessageDialog(this.frame, "Mohon isi seluruh Field");
            }else{
               frame.getContentPane().removeAll();
               new DetailRingkasanMataKuliahGUI(frame, getMatakuliah(), SistemAkademikGUI.getDaftarMahasiswa(), getMatakuliah().getDaftarMahasiswa());
               frame.invalidate();
               frame.validate();
               frame.repaint();
            }
        }
    }

}

    // Uncomment method di bawah jika diperlukan
    /*
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
    */

