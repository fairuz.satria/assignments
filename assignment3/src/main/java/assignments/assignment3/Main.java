package assignments.assignment3;

import java.util.*;
import java.util.stream.Collectors;
import java.lang.*;
import java.io.*;

class SortbyFriendship implements Comparator<ElemenFasilkom>                                                                    // pembuatan class comparator untuk meng compare berdasarkan nilai poin friendship dari yang terbesar
{
    public int compare(ElemenFasilkom a, ElemenFasilkom b)
    {
        return b.getFriendship() - a.getFriendship();
    }
}


public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    int totalMataKuliah = 0;

    int totalElemenFasilkom = 0; 

    void addMahasiswa(String nama, long npm) {                                                                                  // method add mahasiswa                                                        
        /* TODO: implementasikan kode Anda di sini */
        for(int i = 0; i < this.daftarElemenFasilkom.length; i++){                                                              // iterasi sampai menemukan index yang nilainya null
            if(daftarElemenFasilkom[i] == null){
                this.daftarElemenFasilkom[i] = new Mahasiswa(nama, npm);                                                        // mengassign dengan new Mahasiswa
                System.out.println(nama + " berhasil ditambahkan");
                this.totalElemenFasilkom += 1;
                return;
            }
        }
    }

    void addDosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        for(int i = 0; i < this.daftarElemenFasilkom.length; i++){
            if(daftarElemenFasilkom[i] == null){
                this.daftarElemenFasilkom[i] = new Dosen(nama);
                System.out.println(nama + " berhasil ditambahkan");
                this.totalElemenFasilkom += 1;
                return;
            }
        }

    }

    void addElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        for(int i = 0; i < this.daftarElemenFasilkom.length; i++){
            if(daftarElemenFasilkom[i] == null){
                this.daftarElemenFasilkom[i] = new ElemenKantin(nama);
                System.out.println(nama + " berhasil ditambahkan");
                this.totalElemenFasilkom += 1;
                return;
            }
        }
    }

    void menyapa(String objek1, String objek2) {                                                            
        /* TODO: implementasikan kode Anda di sini */   
        if(objek1.equalsIgnoreCase(objek2)){                                                                                    // jika objek 1 dan objek 2 sama
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        }else{
            ElemenFasilkom obj1 = getObjek(objek1);                                                                             // mendapatkan objek meggunakan getObjek()
            ElemenFasilkom obj2 = getObjek(objek2);
            if(obj1.menyapa(obj2)){
                obj2.menyapa(obj1);
                System.out.println(obj1.getNama() + " menyapa dengan " + obj2.getNama());
            }
        }
    }

    void addMakanan(String objek, String nama, long harga) {                                                                     
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = getObjek(objek);
        if(obj.getTipe().equals("Elemen Kantin")){                                                                              // hanya elemen kantin yang dapat menambahkan makanan
            ElemenKantin obj1 = (ElemenKantin) obj;                                                                             // downcasting untuk keperluan pemanggilan method
            obj1.setMakanan(nama, harga);
        }else{
            System.out.println("[DITOLAK] " + obj.getNama() + " bukan merupakan elemen kantin");
        }
    }

    void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj1 = getObjek(objek1);
        ElemenFasilkom obj2 = getObjek(objek2);
        if(obj2.getTipe().equals("Elemen Kantin")){                                                                             // hanya elemen kantin yang dapat menjadi penjual makanan
            if(obj1 != obj2){                                                                                                   // objek harus berbeda
                obj1.membeliMakanan(obj1, obj2, namaMakanan);
            }else{
                System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            }
        }else{
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
    }

    void createMatkul(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        for(int i = 0; i < this.daftarMataKuliah.length; i++){
            if(this.daftarMataKuliah[i] == null){
                this.daftarMataKuliah[i] = new MataKuliah(nama, kapasitas);
                System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
                return;
            }
        }
    }

    void addMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = getObjek(objek);
        if(obj.getTipe().equals("Mahasiswa")){
            Mahasiswa obj1 = (Mahasiswa) obj;
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);                                                              // mendapatkan matakuliah dengan method getMataKuliah()
            obj1.addMatkul(mataKuliah);                                                                                         // memanggul method addmatkul pada mahasiswa
        }else{
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }
    }

    void dropMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = getObjek(objek);
        if(obj.getTipe().equals("Mahasiswa")){
            Mahasiswa obj1 = (Mahasiswa) obj;
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            obj1.dropMatkul(mataKuliah);
        }else{
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        }
    }

    void mengajarMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = getObjek(objek);
        if(obj.getTipe().equals("Dosen")){
            Dosen obj1 = (Dosen) obj;
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            obj1.mengajarMataKuliah(mataKuliah);
        }else{
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }

    }

    void berhentiMengajar(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = getObjek(objek);
        if(obj.getTipe().equals("Dosen")){
            Dosen obj1 = (Dosen) obj;
            obj1.dropMataKuliah();
        }else{
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
    }

    void ringkasanMahasiswa(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = getObjek(objek);
        if(obj.getTipe().equals("Mahasiswa")){
            Mahasiswa obj1 = (Mahasiswa) obj;
            String temp = "Nama: " + obj.getNama() + "\nTanggal lahir: " + obj1.getTanggalLahir() + "\nJurusan: " + obj1.getJurusan() + "\nDaftar Mata Kuliah:";
            System.out.println(temp);
            MataKuliah[] daf = obj1.getDaftarMataKuliah();
            if(daf[0] == null){                                                                                                 // mendeteksi apakah sudah ada matakuliah apa belum
                System.out.println("Belum ada mata kuliah yang diambil");
            }else{
                for(int i = 0; i < daf.length; i++){
                    if(daf[i] == null){
                        break;
                    }
                    int sementara = i+1;
                    System.out.println(sementara + ". " + daf[i]);
                }
            }
        }else{
            System.out.println("[DITOLAK] " + obj.getNama() + " bukan merupakan seorang mahasiswa");
        }
    }

    void ringkasanMataKuliah(String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

        String pengajar = "";                                                                                                   // mendeteksi apakah sudah ada pengajar/dosen nya apa belum
        if(mataKuliah.getDosen() == null){
            pengajar = "Belum ada";
        }else{
            pengajar = mataKuliah.getDosen().getNama();
        }
        
        String temp = "Nama mata kuliah: " + mataKuliah.getNama() + "\nJumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa() + "\nKapasitas: " + mataKuliah.getKapasitas() +
                        "\nDosen pengajar: " + pengajar + "\nDaftar mahasiswa yang mengambil mata kuliah ini:";
        System.out.println(temp);
        if(mataKuliah.getJumlahMahasiswa() == 0){                                                                               // mendeteksi apakah sudah ada mahasiswa nya atau belum
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        }else{
            for(int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++){
                int sementara = i+1;
                System.out.println(sementara + ". " + mataKuliah.getDaftarMahasiswa()[i]);
            }
        }
    }

    void nextDay() {
        /* TODO: implementasikan kode Anda di sini */
        for(ElemenFasilkom x : this.daftarElemenFasilkom){
            if(x == null){
                break;
            }
            if(this.totalElemenFasilkom % 2 == 0){                                                                              // mendeteksi apakah objek sudah menyapa lebih dari setengah atau belum dibagi menjadi 2 cara yaitu bila ganjil dan bila genap
                if(x.getJumlahMenyapa() >= (this.totalElemenFasilkom)/2){
                    x.addFriendship(10);
                }else{
                    x.addFriendship(-5);
                }
            }else{
                if(x.getJumlahMenyapa() >= (this.totalElemenFasilkom-1)/2){
                    x.addFriendship(10);
                }else{
                    x.addFriendship(-5);
                }

            }
            if(x.getFriendship() > 100){                                                                                        // jika poin friendship lebih dari 100 atau kurang dari 0
                x.setFriendship(100);
            }else if(x.getFriendship() < 0){
                x.setFriendship(0);
            }
            
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
        for(ElemenFasilkom x : this.daftarElemenFasilkom){
            if(x == null){
                break;
            }
            x.resetMenyapa();
        }
    }

    void friendshipRanking() {
        /* TODO: implementasikan kode Anda di sini */

        List<ElemenFasilkom> peringkat = new ArrayList<ElemenFasilkom>();                                                       // membuat List baru yang berisi seluruh elemenfailkom tanpa null dari daftarElemenFasilkom
        for(int i = 0; i < this.daftarElemenFasilkom.length; i++){
            if(this.daftarElemenFasilkom[i] == null){
                break;
            }
            peringkat.add(this.daftarElemenFasilkom[i]);
        }
        Comparator<ElemenFasilkom> compareByFriendship = new SortbyFriendship();                                                // membuat comparator dengan pertama di compare poin friendship lalu di compare alphabet nya
        
        Comparator<ElemenFasilkom> compareByAlphabet = Comparator.comparing( ElemenFasilkom::getNama );
        
        Comparator<ElemenFasilkom> compareAll = compareByFriendship.thenComparing(compareByAlphabet);

        List<ElemenFasilkom> peringkatsorted = peringkat.stream().sorted(compareAll).collect(Collectors.toList());

        for(int i = 0; i < peringkatsorted.size(); i++){
            int sementara = i+1;
            System.out.println(sementara + ". " + peringkatsorted.get(i) + "(" + peringkatsorted.get(i).getFriendship() + ")"); 
        }
    }

    void programEnd() {                                                                                                         // method exit program dan memanggil method friendshipranking
        /* TODO: implementasikan kode Anda di sini */
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
        System.exit(0);

        
    }

    private ElemenFasilkom getObjek(String nama){                                                                               // method untuk mendapatkan objek dari namanya
        for(ElemenFasilkom x : this.daftarElemenFasilkom){
            if(x == null){
                break;
            }
            if(x.getNama().equalsIgnoreCase(nama)){
                return x;
            }
        }
        return null;
    }

    private MataKuliah getMataKuliah(String nama){                                                                              // method mendapatkan matakuliah dari namanya
        for(MataKuliah x : this.daftarMataKuliah){
            if(x == null){
                break;
            }
            if(x.getNama().equals(nama)){
                return x;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        Main program = new Main();
        program.run();
    }

    private void run(){
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }

        input.close();
    }
}
