package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI {

    private JComboBox[] acComboBoxs = new JComboBox[2];

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        Mahasiswa[] tempmMahasiswas = new Mahasiswa[SistemAkademikGUI.getDaftarMahasiswa().size()];
        MataKuliah[] tempKuliahs = new MataKuliah[SistemAkademikGUI.getDaftarMataKuliah().size()];
        SistemAkademikGUI.getDaftarMahasiswa().toArray(tempmMahasiswas);
        SistemAkademikGUI.getDaftarMataKuliah().toArray(tempKuliahs);
        acComboBoxs[0] = new JComboBox<>(tempmMahasiswas);
        acComboBoxs[1] = new JComboBox<>(tempKuliahs);
        
        Container container = frame.getContentPane();
        JLabel titLabel = new JLabel("Hapus IRS");
        titLabel.setFont(SistemAkademikGUI.fontTitle);
        titLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel[] aLabels = {new JLabel("Pilih NPM"), new JLabel("Pilih Nama Matkul")};

        JButton kembali = new JButton("Kembali");
        JButton hapus = new JButton("Hapus");
        hapus.setFont(SistemAkademikGUI.fontGeneral);
        hapus.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);

        kembali.addActionListener(new Actions(frame, "home"));
        hapus.addActionListener(new hapusIRS(frame));

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        JLabel[] space = new JLabel[11];
        for(int i = 0; i < 11; i++){
            space[i] = new JLabel(" ");
        }

        container.add(titLabel);
        container.add(space[0]);

        for(int i = 0; i < 2; i++){
            aLabels[i].setFont(SistemAkademikGUI.fontGeneral);
            aLabels[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            acComboBoxs[i].setMaximumSize(new Dimension(200, 24));
            acComboBoxs[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            container.add(aLabels[i]);
            container.add(space[(2*i)+1]);
            container.add(acComboBoxs[i]);
            container.add(space[(2*i)+2]);
        }

        container.add(hapus);
        container.add(space[5]);
        container.add(kembali);
    }


    // Uncomment method di bawah jika diperlukan
    private MataKuliah getMataKuliah() {
        return (MataKuliah) acComboBoxs[1].getSelectedItem();
    }

    private Mahasiswa getMahasiswa() {
        return (Mahasiswa) acComboBoxs[0].getSelectedItem();
    }
    class hapusIRS implements ActionListener{

        JFrame frame;
    
        public hapusIRS(JFrame frame){
            this.frame = frame;
        }
    
        public void actionPerformed(ActionEvent e){
            if(getMahasiswa() == null || getMataKuliah() == null){
                JOptionPane.showMessageDialog(this.frame, "Mohon isi seluruh Field");
            }else{
                String temp = getMahasiswa().dropMatkul(getMataKuliah());
                JOptionPane.showMessageDialog(this.frame, temp);
            }
        }

    }
}


