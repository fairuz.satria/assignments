package assignments.assignment2;


import java.util.Scanner;

public class ExtractNPM {

    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the valgidation
    - and so on
    */

    public static boolean validate(long npm) {                                                              // method validasi
        String x = Long.toString(npm);                                                                      // long ke string
        int masuk = Integer.parseInt(x.substring(0,2));                                                     // memisah2 string ke beberapa variabel
        Integer kjurus = Integer.valueOf(x.substring(2,4));
        int urut = Integer.parseInt(x.substring(12,13));
        if(x.length() == 14){                                                                               // jika panjang sring npm adalah 14
            if(MasukVal(masuk) && JurusVal(kjurus) && DateVal(x) && UniqVal(x) && UrutVal(urut)){
                return true;
            }
        }
        return false;
    }

    public static String extract(long npm) {                                                                // method extract return string
        String x = Long.toString(npm);                                                                      // Long ke string
        String masuk = x.substring(0,2);                                                                    // memisah2 string ke beberapa variabel
        String jurus = x.substring(2,4);
        String tahun = x.substring(8,12);
        String bulan = x.substring(6,8);
        String hari = x.substring(4, 6);
        String jurusstr = "";
        switch(jurus){                                                                                      // switch dengan parameter jurus                                                                                                           //
            case "01":                                                                                      // jurusstr di assign seusai kode jurusan
                jurusstr = "IK" ;
                break;
            case "02":
                jurusstr = "SI";
                break;
            default:
            ;
        }
        String all = "Tahun masuk: 20" + masuk + "\nJurusan: " + jurusstr + "\nTanggal Lahir: " + hari + "-" +  // Seluruh string disatukan
                        bulan + "-" + tahun;
        // if(validate(npm)){                                                                                      // Validasi dengan method validate
        //     return jurusstr;
        // }
        return jurusstr;                                                                    // jika tid
    }

    public static Integer UniqNumNext(Integer x1){                                                              // method uniq number tahap 2
        Integer x2 = 0;
        String x = x1.toString();
        for(int i = 0; i <= x.length()-1; i++){                                                                 // menambah tiap2 angka sesuai urutan
            x2 += Integer.parseInt(x.substring(i,i+1));
        }
        if(x2 <= 10){                                                                                           // jika angka uniq sudah kurang dari 10
            return x2;
        }else{
            return UniqNumNext(x2);                                                                             // jika belum, rekursif
        }
    }

    public static Integer UniqNum(String x1){                                                                   // method uniq number tahap 1
        Integer uniqnum = 0;
        int uniqnumindx = x1.length()-1;
        for(int i = 0; i <= (x1.length()-1)/2; i++){                                                            // iterasi uniq num
           uniqnum += Integer.parseInt(x1.substring(i,i+1))*Integer.parseInt(x1.substring(uniqnumindx-i,(uniqnumindx-i)+1)); // pengoprasian uniq num seusai ketentuan
        }
        uniqnum += Integer.parseInt(x1.substring(6,7));
        uniqnum = UniqNumNext(uniqnum);                                                                         
        return uniqnum;                                                                                          // return nilai akhir uniq number                         
    }

    public static boolean UniqVal(String x1){                                                                    // method mengecek apakah uniq num yang di input sama dengan uniq num yang di return dari method uniqnum
        String uniq = x1.substring(0,13);
        if(UniqNum(uniq) == Integer.valueOf(x1.substring(13,14))){                                               // jika sama
            return true;
        }
        return false;
    }
        
    public static boolean UrutVal(int x1){                                                                       // method mengecek urutan tanggal lahir sama
        if(x1 >= 1){
            return true;
        }
        return false;
    }

    public static boolean DateVal(String x1){                                                                    // pengecekan umur
        int masuk = Integer.parseInt(x1.substring(0,2)) + 2000;
        int tahun = Integer.parseInt(x1.substring(8,12));
        if(masuk - tahun >= 15){                                                                                 // tahun masuk - tahun lahir
            return true;
        }
        return false;
    }

    public static boolean MasukVal(int x1){                                                                      // pengecekan tahun masuk tidak kurang dari 10 atau 2010
        if(x1 < 21 && x1 >= 10){
            return true;
        }
        return false;
    }

    public static boolean JurusVal(Integer x1){                                                                  // pengecekan apakah nomor jurusan terdapat dalam daftar juruan
        if(x1 == 01 || x1 == 02){
            return true;
        }
        return false;
    }


    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }else{
                System.out.println(extract(npm));
            }
        }


            // TODO: Check validate and extract NPM
            
        input.close();
    }
}