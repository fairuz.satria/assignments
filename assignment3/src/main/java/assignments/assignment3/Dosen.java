package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    MataKuliah mataKuliah;

    Dosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        setNama(nama);                                                                                                          // saat construct dipanggil method set akan dipanggil untuk mengassign parameter ke private instance di abstract class elemenfasilkom
        setTipe("Dosen");
    }

    void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */     
        if(this.mataKuliah == null){                                                                                            // jika belum mengajar
            if(mataKuliah.getDosen() == null){                                                                                  // jika matakuliah belum punya dosen
                this.mataKuliah = mataKuliah;                                                                                   // matakuliah di assign dan dosen di assign ke matakuliah
                mataKuliah.addDosen(this);
                System.out.println(getNama() + " mengajar mata kuliah " + mataKuliah.getNama());
            }else{
                System.out.println("[DITOLAK] " + mataKuliah.getNama() + " sudah memiliki dosen pengajar");
            }

        }else{
            System.out.println("[DITOLAK] " + getNama() + " sudah mengajar mata kuliah " + this.mataKuliah.getNama());
        }
    }

    void dropMataKuliah() {
        /* TODO: implementasikan kode Anda di sini */
        if(this.mataKuliah == null){
            System.out.println("[DITOLAK] " + getNama() + " sedang tidak mengajar mata kuliah apapun");
        } else{
            this.mataKuliah.dropDosen();                                                                                        // matakuliah di assign null juga dosen di matakuliah
            System.out.println(getNama() + " berhenti mengajar " + this.mataKuliah.getNama());
            this.mataKuliah = null;
        }
    }

    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }
}