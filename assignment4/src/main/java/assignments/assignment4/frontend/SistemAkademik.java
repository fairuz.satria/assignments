package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class SistemAkademik {

    public static void main(String[] args) { 
        new SistemAkademikGUI();
    }
}
class SistemAkademikGUI extends JFrame{
    private static ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();
    private static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();
    public static Font fontGeneral = new Font("Century Gothic", Font.PLAIN , 14);
    public static Font fontTitle = new Font("Century Gothic", Font.BOLD, 20);

    public SistemAkademikGUI(){

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        // TODO: Tambahkan hal-hal lain yang diperlukan
        
        new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
        frame.setVisible(true);
    }

    public static ArrayList<Mahasiswa> getDaftarMahasiswa() {
        return daftarMahasiswa;
    }

    public static ArrayList<MataKuliah> getDaftarMataKuliah() {
        return daftarMataKuliah;
    }

    public static void addDaftarMahasiswa(Mahasiswa mahasiswa) {
        SistemAkademikGUI.daftarMahasiswa.add(mahasiswa);
    }

    public static void addDaftarMatakuliah(MataKuliah mataKuliah) {
        SistemAkademikGUI.daftarMataKuliah.add(mataKuliah);
    }

    public static Mahasiswa getMahasiswa(String x){
        for(Mahasiswa x1: daftarMahasiswa){
            if(x.equals(x1.getNama())){
                return x1;
            }
        }
        return null;
    }

    public static void sortmhs(ArrayList<Mahasiswa> daftarMahasiswa){
        boolean temp = true;
        while(temp){
            temp = false;
            for(int i = 0; i < daftarMahasiswa.size() - 1; i++){
                Mahasiswa now = daftarMahasiswa.get(i);
                Mahasiswa next = daftarMahasiswa.get(i+1);
                if(now.getNpm() > next.getNpm()){
                    daftarMahasiswa.set(i, next);
                    daftarMahasiswa.set(i+1, now);
                    temp = true;
                }
            }
        }
    }

    public static void sortmatkul(ArrayList<MataKuliah> daftarMataKuliah){
        boolean temp = true;
        while(temp){
            temp = false;
            for(int i = 0; i < daftarMataKuliah.size() - 1; i++){
                MataKuliah now = daftarMataKuliah.get(i);
                MataKuliah next = daftarMataKuliah.get(i+1);
                char nowc = now.getNama().toLowerCase().charAt(0);
                char nextc = next.getNama().toLowerCase().charAt(0);
                if(nowc > nextc){
                    daftarMataKuliah.set(i, next);
                    daftarMataKuliah.set(i+1, now);
                    temp = true;
                }
            }
        }
    }



}
