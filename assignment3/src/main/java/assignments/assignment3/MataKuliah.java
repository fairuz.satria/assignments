package assignments.assignment3;

class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa = {};

    private int jumlahMahasiswa;

    MataKuliah(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kapasitas = kapasitas;
    }

    void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        this.daftarMahasiswa = this.adds(this.daftarMahasiswa, mahasiswa);
        this.jumlahMahasiswa += 1;

    }

    void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        this.daftarMahasiswa = this.removes(this.daftarMahasiswa, mahasiswa);
        this.jumlahMahasiswa -= 1;
    }

    void addDosen(Dosen dosen) {
        /* TODO: implementasikan kode Anda di sini */
        this.dosen = dosen;
    }

    void dropDosen() {
        /* TODO: implementasikan kode Anda di sini */
        this.dosen = null;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    public String getNama() {                                                                   // setter getter
        return nama;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return daftarMahasiswa;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public int getJumlahMahasiswa() {
        return jumlahMahasiswa;
    }

    public Mahasiswa[] adds(Mahasiswa[] x, Mahasiswa x1){                                       // method menambahkan mahasiswa baru kedalam array
        Mahasiswa[] newa = new Mahasiswa[x.length + 1];                                         // membuat array dengan panjang array sebelumnya + 1
        for(int i = 0; i < x.length; i++){                                                      // mengassign array baru sama dengan array lama
            newa[i] = x[i];
        }
        newa[x.length] = x1;                                                                    // mengassign array index terakhir dengan mahasiswa baru
        return newa;
    }
    
    public Mahasiswa[] removes(Mahasiswa[] x, Mahasiswa x1){                                    // method remove mahasiswa yang diinginkan
        Mahasiswa[] newa = new Mahasiswa[x.length - 1];                                         // membuat array dengan panjang array sebelumnya + 1
        for(int i = 0, j = 0; j < newa.length; i++){                            // jika array dengan index sama dengan mahasiswa yang ingin di remove maka di continue
            if(x[i] == x1){
                continue;
            } else{                                                                             // jika tidak maka di assign
                newa[j] = x[i];
                j++;
            }
        }
        return newa;
    }

    public Dosen getDosen() {
        return dosen;
    }
}