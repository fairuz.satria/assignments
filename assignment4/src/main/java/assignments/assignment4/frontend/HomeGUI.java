package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        Container container = frame.getContentPane();                                                                    // mendapatkan container dari jframe untuk ditambahkan object didalalammnya
        JButton button1 = new JButton("Tambah Mahasiswa");                                                               // membuat button dengan text tertentu
        JButton button2 = new JButton("Tambah Mata Kuliah");
        JButton button3 = new JButton("Tambah IRS");
        JButton button4 = new JButton("Hapus IRS");
        JButton button5 = new JButton("Lihat Ringkasan Mahasiswa");
        JButton button6 = new JButton("Lihat Ringkasan Mata Kuliah");
        JButton[] allButtons = {button1, button2, button3, button4, button5, button6};                                   // memasukan seluruh  button ke dalam array 

        JLabel titleLabel = new JLabel();                                                                                // membuat Jlabel untuk judul
        JLabel kosong = new JLabel(" ");
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);                                                                 // mangatur font dari text

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));                                                 // mengatur jenis layout yang akan dipakai untuk penempatan object
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);                                                            // mengatur posisi object
        container.add(titleLabel);                                                                                       // menambahkan object kedalam container
        container.add(kosong);
        JLabel[] temp = new JLabel[6];
        for(int i = 0; i < 6; i++){
            allButtons[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            allButtons[i].setBackground(Color.GREEN);
            allButtons[i].setForeground(Color.WHITE);                                                                    // menambahkan warna ke text didalam suatu object
            temp[i] = new JLabel();
            temp[i].setText(" ");;
            container.add(allButtons[i]);
            container.add(temp[i]);
        }
        button1.addActionListener(new Actions(frame, "tambahmahasiswa"));                                               // menambahkan action listener saat button di tekan
        button2.addActionListener(new Actions(frame, "tambahmatkul"));
        button3.addActionListener(new Actions(frame, "tambahirs"));
        button4.addActionListener(new Actions(frame, "hapusirs"));
        button5.addActionListener(new Actions(frame, "ringkasanmahasiswa"));
        button6.addActionListener(new Actions(frame, "ringkasanmatakuliah"));
    }
}

class Actions implements ActionListener{                                                                                 // class action listener     
    JFrame frame;
    String guit;

    public Actions(JFrame frame, String guit){
        this.frame = frame;
        this.guit = guit;
    }

    public void actionPerformed(ActionEvent e){                                                                          // saat tombbol di tekan
        frame.getContentPane().removeAll();                                                                              // mengahapus semua object yang sebelumnya sudah ditambahkan
        if(guit.equals("tambahmahasiswa")){
            new TambahMahasiswaGUI(frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMataKuliah()); // ditambahkan object baru dengan memanggil constructor yang suah di bentuk
            frame.invalidate();                                                                                          // setelah dihapus dan ditambahkan me-refresh frame agar object baru terlihat
            frame.validate();
            frame.repaint();
        }else if(guit.equals("tambahmatkul")){
            new TambahMataKuliahGUI(frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMataKuliah());
            frame.invalidate();
            frame.validate();
            frame.repaint();
        }else if(guit.equals("tambahirs")){
            new TambahIRSGUI(frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMataKuliah());
            frame.invalidate();
            frame.validate();
            frame.repaint();
        }else if(guit.equals("hapusirs")){
            new HapusIRSGUI(frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMataKuliah());
            frame.invalidate();
            frame.validate();
            frame.repaint();
        }else if(guit.equals("ringkasanmahasiswa")){
            new RingkasanMahasiswaGUI(frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMataKuliah());
            frame.invalidate();
            frame.validate();
            frame.repaint();
        }else if(guit.equals("home")){
            new HomeGUI(frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMataKuliah());
            frame.invalidate();
            frame.validate();
            frame.repaint();
        }else{
            new RingkasanMataKuliahGUI(frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMataKuliah());
            frame.invalidate();
            frame.validate();
            frame.repaint();
        }
    }
}
