package assignments.assignment3;

import java.util.*;

class ElemenKantin extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    Makanan[] daftarMakanan = new Makanan[10];

    

    ElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        setNama(nama);
        setTipe("Elemen Kantin");
    }

    void setMakanan(String nama, long harga) {                                                                                                       // Menambahkan makanan kedalam array Makanan[] jika makanan belum ada di dalam array 
        /* TODO: implementasikan kode Anda di sini */
        if(exist(nama)){
            System.out.println("[DITOLAK] " + nama + " sudah pernah terdaftar");
        }else{
            this.daftarMakanan = addMakanans(this.daftarMakanan, new Makanan(nama, harga));
            System.out.println(getNama() + " telah mendaftarkan makanan " + nama + " dengan harga " + harga);
        }

    }

    public Makanan[] getDaftarMakanan() {
        return daftarMakanan;
    }

    private boolean exist(String nama){                                                                                                             // method mengecek apakah makanan sudah ada atau belum di daftar makanan dengan parameter string
        for(int i = 0; i < daftarMakanan.length; i++){
            if(daftarMakanan[i] == null){
                break;
            }
            if(daftarMakanan[i].getNama().equalsIgnoreCase(nama)){
                return true;
            }
        }
        return false;
    }

    private Makanan[] addMakanans(Makanan[] x, Makanan x1){                                                                                         //METHOD untuk menambahkan kan makanan ke Makanan array dengan panjang yang telah ditentukan dan sudah diinisiasi
        for(int i = 0; i < x.length; i++){
            if(x[i] == null){
                x[i] = x1;
                break;
            }
        }
        return x;
    }
}