package assignments.assignment3;

abstract class ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String tipe;
    
    private String nama;

    private int friendship;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    private int jumlahMenyapa;
    

    boolean menyapa(ElemenFasilkom elemenFasilkom) {                                                                                                                // rangkaian method menyapa
        /* TODO: implementasikan kode Anda di sini */
        if(bosan(telahMenyapa, elemenFasilkom)){                                                                                                                    // jika sudah disapa
            System.out.println("[DITOLAK] " + this.nama + " telah menyapa " + elemenFasilkom.getNama() + " hari ini");
            return false;
        } else{                                                                                                                                                     // jika belum
            add(elemenFasilkom);
            this.jumlahMenyapa += 1;
            if(elemenFasilkom.getTipe().equals("Mahasiswa")){                                                                                                       // penambahan poin friendship sesuai ketentuan
                Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom;
                if(this.getTipe().equals("Dosen")){
                    Dosen dosen = (Dosen) this;
                    boolean semen = mahasiswa.diajar(dosen);
                    if(semen){
                        mahasiswa.addFriendship(2);
                        dosen.addFriendship(2);
                    }
                }
            }
            return true;
        }
    }

    void resetMenyapa() {                                                                                                                                           // mereset array dengan membuat baru
        /* TODO: implementasikan kode Anda di sini */
        this.telahMenyapa = new ElemenFasilkom[100];
        this.jumlahMenyapa = 0;

    }

    void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenKantin penjual1 = (ElemenKantin) penjual;                                                                                                             // downcasting untuk keperluan memanggil method
        for(int i = 0; i < penjual1.daftarMakanan.length; i++){
            if(penjual1.daftarMakanan[i] == null){
                break;
            }
            if(namaMakanan.equals(penjual1.daftarMakanan[i].getNama())){
                System.out.println(pembeli.getNama() + " berhasil membeli " + namaMakanan + " seharga " + penjual1.daftarMakanan[i].harga);
                penjual.addFriendship(1);
                pembeli.addFriendship(1);
                return;
            }
        }
        System.out.println("[DITOLAK] " + penjual.getNama() + " tidak menjual " + namaMakanan);


    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    void add(ElemenFasilkom x1){                                                                                                                                    // method menambahkan objek baru kedalam array yang berisi panjang tertentu yaitu telahMenyapa
        for(int i = 0; i < this.telahMenyapa.length; i++){
            if(this.telahMenyapa[i] == null){
                this.telahMenyapa[i] = x1;
                return;
            }
        }
    }

    boolean bosan(ElemenFasilkom[] x, ElemenFasilkom x1){                                                                                                           // method pengecekan apakah sudah terdapat objek tertentu di dalam array telah menyapa
        for(int i = 0; i < x.length; i++){                                                                                                                          // menggunakan iterasi
            if(x[i] == null){
                break;
            }
            if(x1 == x[i]){
                return true;
            }
        }
        return false;
    }

    public String getNama() {                                                                                                                                       // setter getter
        return nama;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe){
        this.tipe = tipe;
    }

    public int getFriendship() {
        return friendship;
    }

    public void addFriendship(int friendship) {
        this.friendship += friendship;
    }

    public void setFriendship(int friendship) {
        this.friendship = friendship;
    }

    public int getJumlahMenyapa() {
        return jumlahMenyapa;
    }

    public ElemenFasilkom[] getTelahMenyapa() {
        return telahMenyapa;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}