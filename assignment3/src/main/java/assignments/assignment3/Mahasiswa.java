package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;


    Mahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        setNama(nama);                                                                                                                                    // saat construct dipanggil method set akan dipanggil untuk mengassign parameter ke private instance di abstract class elemenfasilkom
        this.npm = npm;
        this.tanggalLahir = extractTanggalLahir(this.npm);
        this.jurusan = extractJurusan(this.npm);
        setTipe("Mahasiswa");
    }

    void addMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        if(this.already(this.daftarMataKuliah, mataKuliah)){                                                                                              // memanggil method boolean already
            System.out.println(String.format("[DITOLAK] %s telah diambil sebelumnya", mataKuliah.getNama()));
        }else{
            if(this.toKapasitas(mataKuliah)){  
                for(int i = 0; i < this.daftarMataKuliah.length; i++){                                                                                    // iterasi matakluiah array
                    if(this.daftarMataKuliah[i] == null){                                                                                                 // jika matakuliah index ke i null
                        if(i == 9 && this.daftarMataKuliah[i] != null){                                                                                   // jika i mencapai 9 dan matakuliah index ke i sudah tersi, tidak sama dengam null
                            System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10");
                            break;
                        }else{                                                                                                                            // memanggil method boolean toKapasitas
                            this.daftarMataKuliah[i] = mataKuliah;                                                                                        // menambah mataKuliah ke index i                                                                                                                       // menambah sks
                            mataKuliah.addMahasiswa(this);  
                            System.out.println(this + " berhasil menambahkan mata kuliah " + mataKuliah);                                                 // menmbah mahasiswa ke mataKuliah
                            break;
                        }
                    }
                }
            }
            else{
                System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya", mataKuliah.getNama()));
            }
        }

    }

    public boolean already(MataKuliah[] x, MataKuliah x1){                                                                                                // method already mengecek apakah mataKuliah sudah dalam array
        for(int i = 0; i < x.length; i++){
            if(x[i] == null){
                break;
            }else{
                if(x[i] == x1){                                                                                                                           // jika saat iterasi ketemu
                    return true;
                }
            }
        }
        return false;                                                                                                                                     // jika tidak
    }

    public boolean toKapasitas(MataKuliah x){                                                                                                             // mengecek apakah panjang array sudah melebihi kapasitas
        if(x.getDaftarMahasiswa().length < x.getKapasitas()){
            return true;
        }else{
            return false;
        }
    }

    public static MataKuliah[] removes(MataKuliah[] x, MataKuliah x1){     
        MataKuliah[] temp = new MataKuliah[10];                                                                                                           // method removes seperti yang ada di Mahasiswa.java
        for(int i = 0, j = 0; i < x.length;){
            if(x[i] == null){
                break;
            }else{
                if(x[i] == x1){                                                                                                                           // jika saat iterasi ketemu
                    i++;
                }else{
                    temp[j]  = x[i];
                    j++;
                    i++;
                }
            }
        }
        return temp; 
    }


    void dropMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        if(this.already(this.daftarMataKuliah, mataKuliah)){                                                                                              // memanggil method boolean already
            this.daftarMataKuliah = removes(this.daftarMataKuliah, mataKuliah);                                                                           // me removes mataKuliah dari array
            mataKuliah.dropMahasiswa(this); 
            System.out.println(this.getNama() + " berhasil drop mata kuliah " + mataKuliah.getNama());                                                                                                                                             // drop mahasiswa dari mataKuliah                                                                                                                                        // sks berkurang
        }else{
            System.out.println(String.format("[DITOLAK] %s belum pernah diambil", mataKuliah.getNama()));
        }
    }

    private String extractTanggalLahir(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        String x = Long.toString(npm);                                                                                                                    // Long ke string
        String tahun = x.substring(8,12);
        String bulan = x.substring(6,8);
        String hari = x.substring(4, 6);
        if(hari.substring(0, 1).equals("0")){
            hari = hari.substring(1, 2);
        }
        if(bulan.substring(0, 1).equals("0")){
            bulan = bulan.substring(1, 2);
        }
        return hari + "-" + bulan + "-" + tahun;
    }

    private String extractJurusan(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        String x = Long.toString(npm);                                                                                                                    // Long ke string               
        String jurus = x.substring(2,4);                                                                                                                  // memisah2 string ke beberapa variabel
        String jurusstr = "";
        switch(jurus){                                                                                                                                    // switch dengan parameter jurus                                                                                                           //
            case "01":                                                                                                                                    // jurusstr di assign seusai kode jurusan
                jurusstr = "Ilmu Komputer" ;
                break;
            case "02":
                jurusstr = "Sistem Informasi";
                break;
            default:
            ;
        }
        return jurusstr;
    }

    boolean diajar(Dosen dosen){
        for(int i = 0; i < this.daftarMataKuliah.length; i++){
            if(this.daftarMataKuliah[i] == null){
                break;
            }
            if(dosen.getMataKuliah() == this.daftarMataKuliah[i]){
                return true;
            }
        }
        return false;
    }
    
    public String getJurusan() {                                                                                                                          //setter getter
        return jurusan;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public MataKuliah[] getDaftarMataKuliah() {
        return daftarMataKuliah;
    }

}