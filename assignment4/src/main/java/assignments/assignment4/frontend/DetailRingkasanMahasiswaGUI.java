package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, MataKuliah[] daftarMataKuliah){
        Container container = frame.getContentPane();

        String nama = "Nama: " + mahasiswa.getNama();
        String npm = "NPM: " + mahasiswa.getNpm();
        String jurusan = "Jurusan: " + mahasiswa.getJurusan();
        String sks = "Total SKS: " + mahasiswa.getTotalSKS();

        JLabel titlLabel = new JLabel("Detail RIngkasan Mahasiswa"); titlLabel.setFont(SistemAkademikGUI.fontTitle); titlLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel namaJ = new JLabel(nama); namaJ.setFont(SistemAkademikGUI.fontGeneral); namaJ.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel npmJ = new JLabel(npm); npmJ.setFont(SistemAkademikGUI.fontGeneral); npmJ.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel jurusanJ = new JLabel(jurusan); jurusanJ.setFont(SistemAkademikGUI.fontGeneral); jurusanJ.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel daftarJ = new JLabel("Daftar Mata Kuliah: "); daftarJ.setFont(SistemAkademikGUI.fontGeneral); daftarJ.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel sksJ = new JLabel(sks); sksJ.setFont(SistemAkademikGUI.fontGeneral); sksJ.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel irsJ = new JLabel("Hasil Pengecekan IRS: "); irsJ.setFont(SistemAkademikGUI.fontGeneral); irsJ.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton selesai = new JButton("Selesai"); selesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        selesai.addActionListener(new Actions(frame, "home"));

        JLabel[] space = new JLabel[100];
        for(int i = 0; i < space.length;i++){
            space[i] = new JLabel(" ");
        }

        container.add(titlLabel);
        container.add(space[3]);
        container.add(namaJ);
        container.add(space[0]);
        container.add(npmJ);
        container.add(space[1]);
        container.add(jurusanJ);
        container.add(space[2]);
        container.add(daftarJ);
        container.add(space[4]);

        if(mahasiswa.getBanyakMatkul() == 0){
            JLabel semen = new JLabel("Belum ada mata kuliah yang diambil."); semen.setAlignmentX(Component.CENTER_ALIGNMENT);
            container.add(semen);
            container.add(space[7]);
        }else{
            for(int i = 0; i < mahasiswa.getBanyakMatkul(); i++){
                String mataKuliah = daftarMataKuliah[i].getNama();
                int temp = i+1;
                JLabel tempJ = new JLabel(temp + ". " + mataKuliah); tempJ.setAlignmentX(Component.CENTER_ALIGNMENT);
                container.add(tempJ);
                container.add(space[7+i]);
            }
        }
    

        container.add(sksJ);
        container.add(space[5]);
        container.add(irsJ);
        container.add(space[6]);

        mahasiswa.cekIRS();
        if(mahasiswa.getBanyakMasalahIRS() == 0){
            JLabel tempj = new JLabel("IRS tidak bermasalah"); tempj.setAlignmentX(Component.CENTER_ALIGNMENT);
            container.add(tempj);
            container.add(space[8+mahasiswa.getBanyakMatkul()]);
        }else{
            for(int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++){
                String masalahirs = mahasiswa.getMasalahIRS()[i];
                int temp = i+1;
                JLabel tempj = new JLabel(temp + ". " + masalahirs); tempj.setAlignmentX(Component.CENTER_ALIGNMENT);
                container.add(tempj);
                container.add(space[8+mahasiswa.getBanyakMatkul()+i]);
            }
        }

        container.add(selesai);
    }

}
