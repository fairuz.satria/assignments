package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {
        /* TODO: Implementasikan kode Anda di sini */
        for(int i = 0; i < daftarMahasiswa.length; i++){
            if(daftarMahasiswa[i] == null){
                break;
            }
            if(daftarMahasiswa[i].getNpm() == npm){                                                                            // jika npm mahasiswa dalam array sama dengan npm parameter
                return daftarMahasiswa[i];
            }
        }
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {
        /* TODO: Implementasikan kode Anda di sini */
        for(int i = 0; i < daftarMataKuliah.length; i++){
            if(daftarMataKuliah[i] == null){
                break;
            }
            if(namaMataKuliah.equals(daftarMataKuliah[i].getNama())){                                                          // jika nama mataKuliah iterasi ke i sama dengan nama mataKuliah parameter
                return daftarMataKuliah[i];
            }
        }
        return null;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        try{
            long npm = Long.parseLong(input.nextLine());
            Mahasiswa newa = this.getMahasiswa(npm);
            /* TODO: Implementasikan kode Anda di sini 
            Jangan lupa lakukan validasi apabila banyaknya matkul yang diambil mahasiswa sudah 9*/
            if(newa != null){                                                                                                  // jika getmahasiswa ketemu
                System.out.print("Banyaknya Matkul yang Ditambah: ");
                int banyakMatkul = Integer.parseInt(input.nextLine());
                System.out.println("Masukkan nama matkul yang ditambah");
                for(int i=0; i<banyakMatkul; i++){
                    System.out.print("Nama matakuliah " + i+1 + " : ");
                    String namaMataKuliah = input.nextLine();
                    /* TODO: Implementasikan kode Anda di sini */
                    MataKuliah temp = this.getMataKuliah(namaMataKuliah);
                    newa.addMatkul(temp);                                                                                      // memanggil addMatkul dari mahasiswa
                }
                System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
            }else{
                ;
            }
        }catch(Exception e){
            ;
        }
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        try{
            long npm = Long.parseLong(input.nextLine());

        /* TODO: Implementasikan kode Anda di sini 
            Jangan lupa lakukan validasi apabila mahasiswa belum mengambil mata kuliah*/
            Mahasiswa newa = this.getMahasiswa(npm);                                                                           // mengambil mahasiswa dari array dengan getMahasiswa
            if(newa != null){
                if(newa.getMataKuliah().length != 0){
                    System.out.print("Banyaknya Matkul yang Di-drop: ");
                    int banyakMatkul = Integer.parseInt(input.nextLine());
                    System.out.println("Masukkan nama matkul yang di-drop:");
                    for(int i=0; i<banyakMatkul; i++){
                        System.out.print("Nama matakuliah " + i+1 + " : ");
                        String namaMataKuliah = input.nextLine();
                        /* TODO: Implementasikan kode Anda di sini */
                        MataKuliah temp = this.getMataKuliah(namaMataKuliah);                                                  // mengambil mataKuliah seusai nama dari array dengan getMataKuliah
                        newa.dropMatkul(temp);                                                                                 // memanggil dropMatkul dari newa(mahasiswa)

                    }
                    System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
                }else{
                    System.out.println("DITOLAK] Belum ada mata kuliah yang diambil.");
                }
            }else{
                ;
            }
        } catch(Exception e){
            ;
        }
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa newa = this.getMahasiswa(npm);
        if(newa != null){
            String nama = newa.toString();                                                                                     // memanggil beberapa getter dari newa(mahasiswa)
            String jurusan = newa.getJurusan();
            int totalSKS = newa.getSks();
            MataKuliah[] temp = newa.getMataKuliah();
            // TODO: Isi sesuai format keluaran
            System.out.println("\n--------------------------RINGKASAN--------------------------\n");
            System.out.println("Nama: " + "" + nama);
            System.out.println("NPM: " + npm);
            System.out.println("Jurusan: " + "" + jurusan);
            System.out.println("Daftar Mata Kuliah: ");
            if(temp[0] == null){
                System.out.println("Belum ada mata kuliah yang diambil");                                                      // jika temp(array matakuliah mahasiswa) index ke 0 masih kosong
            }else{
                for(int i = 0; i < temp.length; i++){                                                                          // itereasi untung mencetak tiap2 matakuliah
                    if(temp[i] == null){
                        break;
                    }else{
                        int inttemp = i+1;
                        System.out.println(inttemp + ". " + temp[i]);
                    }
                }
            }
            /* TODO: Cetak daftar mata kuliah 
            Handle kasus jika belum ada mata kuliah yang diambil*/

            System.out.println("Total SKS: " + "" + totalSKS );

            
            System.out.println("Hasil Pengecekan IRS:");
            newa.cekIRS();
            String[] tempMasalahIRS = newa.getMasalahIRS();
            if(tempMasalahIRS.length == 0){                                                                                    // jika tempMahasalahIRS(masalahIRS mahasiswa) masih kosong
                System.out.println("IRS tidak bermasalah.");
            }else{
                for(int i = 0; i < tempMasalahIRS.length; i++){
                    int inttemp = i+1;
                    System.out.println(inttemp + ". " + tempMasalahIRS[i]);
                }
            }
        }else{
            ;
        }
        /* TODO: Cetak hasil cek IRS
        Handle kasus jika IRS tidak bermasalah */
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah temp = this.getMataKuliah(namaMataKuliah);
        String nama = temp.getNama();
        String kode = temp.getKode();
        int SKS = temp.getSks();
        int dafmaha = temp.getDafMahasiswas();
        int kapasitas = temp.getKapasitas();
        Mahasiswa[] dafmahaarr = temp.detMahasiswasArr();
        
        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + nama);
        System.out.println("Kode: " + kode);
        System.out.println("SKS: " + SKS);
        System.out.println("Jumlah mahasiswa: " + dafmaha);
        System.out.println("Kapasitas: " + kapasitas);
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
       /* TODO: Cetak hasil cek IRS
        Handle kasus jika tidak ada mahasiswa yang mengambil */
        if(dafmaha == 0){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        }else{
            for(int i = 0; i < dafmaha; i++){
                int inttemp = i+1;
                System.out.println(inttemp + ". " + dafmahaarr[i]);
            }
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            String nama = dataMatkul[1];
            String kode = dataMatkul[0];
            for(int j = 0; j < daftarMataKuliah.length; j++){
                if(daftarMataKuliah[j] == null){
                    daftarMataKuliah[j] = new MataKuliah(kode, nama, sks, kapasitas);
                    break;
                }
            }
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            String nama = dataMahasiswa[0];
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            for(int j = 0; j < daftarMahasiswa.length; j++){
                if(daftarMahasiswa[j] == null){
                    daftarMahasiswa[j] = new Mahasiswa(nama, npm);
                    break;
                }
            }
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
