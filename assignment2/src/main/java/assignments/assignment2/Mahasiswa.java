package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS = {};
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    public Mahasiswa(String nama, long npm){                                                                                                                                             // constructor dengan paramter
        this.nama = nama;
        this.npm = npm;
        this.jurusan = ExtractNPM.extract(this.npm);
        /* TODO: implementasikan kode Anda di sini */
    }

    public String getJurusan(){                                                                                                                                                          // method mendapatkan nama jurusan dari kode
        if(this.jurusan.equals("SI")){
            return "Sistem Informasi";
        }else{
            return "Ilmu Komputer";
        }
    }
    
    public void addMatkul(MataKuliah mataKuliah){                                                                                                                                       // method void add matkul
        /* TODO: implementasikan kode Anda di sini */
        for(int i = 0; i < this.mataKuliah.length; i++){                                                                                                                                // iterasi matakluiah array
            if(this.already(this.mataKuliah, mataKuliah)){                                                                                                                              // memanggil method boolean already
                System.out.println(String.format("[DITOLAK] %s telah diambil sebelumnya.", mataKuliah.getNama()));
                break;
            }else{
                if(this.mataKuliah[i] == null){                                                                                                                                         // jika matakuliah index ke i null
                    if(i == 9 && this.mataKuliah[i] != null){                                                                                                                           // jika i mencapai 9 dan matakuliah index ke i sudah tersi, tidak sama dengam null
                        System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                        break;
                    }else{
                        if(this.toKapasitas(mataKuliah)){                                                                                                                               // memanggil method boolean toKapasitas
                            this.mataKuliah[i] = mataKuliah;                                                                                                                            // menambah mataKuliah ke index i
                            this.totalSKS += mataKuliah.getSks();                                                                                                                       // menambah sks
                            mataKuliah.addMahasiswa(this);                                                                                                                              // menmbah mahasiswa ke mataKuliah
                            break;
                        }else{
                            System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya.", mataKuliah.getNama()));
                            break;
                        }
                    }
                }
            }
        }
    }

    public void dropMatkul(MataKuliah mataKuliah){                                                                                                                                       // method void dropMatkul
        /* TODO: implementasikan kode Anda di sini */
        if(this.already(this.mataKuliah, mataKuliah)){                                                                                                                                   // memanggil method boolean already
            this.mataKuliah = removes(this.mataKuliah, mataKuliah);                                                                                                                      // me removes mataKuliah dari array
            mataKuliah.dropMahasiswa(this);                                                                                                                                              // drop mahasiswa dari mataKuliah
            this.totalSKS -= mataKuliah.getSks();                                                                                                                                        // sks berkurang
        }else{
            System.out.println(String.format("[DITOLAK] %s belum pernah diambil.", mataKuliah.getNama()));
        }
    }

    public boolean already(MataKuliah[] x, MataKuliah x1){                                                                                                                               // method already mengecek apakah mataKuliah sudah dalam array
        for(int i = 0; i < x.length; i++){
            if(x[i] == null){
                break;
            }else{
                if(x[i] == x1){                                                                                                                                                          // jika saat iterasi ketemu
                    return true;
                }
            }
        }
        return false;                                                                                                                                                                   // jika tidak
    }


    public void cekIRS(){                                                                                                                                                               // method void cekIRS
        /* TODO: implementasikan kode Anda di sini */
        this.masalahIRS = this.cleanStrings(this.masalahIRS);
        this.toSks();
        this.matchMajor();
    }

    public String toString() {                                                                                                                                                         // to string nama
        /* TODO: implementasikan kode Anda di sini */
        return nama;
    }

    public boolean toKapasitas(MataKuliah x){                                                                                                                                          // mengecek apakah panjang array sudah melebihi kapasitas
        if(x.getDafMahasiswas() < x.getKapasitas()){
            return true;
        }else{
            return false;
        }
    }

    public static MataKuliah[] removes(MataKuliah[] x, MataKuliah x1){                                                                                                                  // method removes seperti yang ada di Mahasiswa.java
        MataKuliah[] newa = new MataKuliah[x.length - 1];
        for(int i = 0, j = 0; i < x.length && j < newa.length; i++){
            if(x[i] == x1){
                continue;
            } else{
                newa[j] = x[i];
                j++;
            }
        }
        return newa;
    }

    public static String[] adds(String[] x, String x1){                                                                                                                                 // method adds seperti yang ada di Mahasiswa.java

        String[] newa = new String[x.length + 1];
        for(int i = 0; i < x.length; i++){
            newa[i] = x[i];
        }
        newa[x.length] = x1;
        return newa;
    }

    public String[] cleanStrings(String[] x){                                                                                                                                           // mengosongkan array string
        String[] newa = {};
        return newa;
    }

    public void toSks(){                                                                                                                                                                // mengecek jika total sks melebihi 24 dan menambahkan peringatan di array masalahIRS
        if(this.totalSKS > 24){
            this.masalahIRS = adds(this.masalahIRS, "SKS yang diambil lebih dari 24");
        }
    }

    public void matchMajor(){                                                                                                                                                           // method mencocokan jurusan matakuliah deengan mahasiswa
        for(int i = 0; i < this.mataKuliah.length; i++){                                                                                                                                  
            if(this.mataKuliah[i] == null){
                break;
            }else{
                if(ExtractNPM.extract(this.npm).equals(this.mataKuliah[i].getKode()) || this.mataKuliah[i].getKode().equals("CS")){                                                     // jika kode jurusan matakuliah sama dengan kode jurusan mahasiswa yang didapar dari npm / kode matakluah = "CS"
                    continue;
                }else{
                    this.masalahIRS = adds(this.masalahIRS, String.format("Mata Kuliah %s tidak dapat diambil jurusan %s", this.mataKuliah[i].getNama(), this.jurusan)); 
                }
            }
        }
    }

    public String getNama(){                                                                                                                                                            // GETTER
        return this.nama;
    }

    public Long getNpm(){
        return this.npm;
    }

    public MataKuliah[] getMataKuliah(){
        return this.mataKuliah;
    }

    public String[] getMasalahIRS(){
        return this.masalahIRS;
    }

    public int getSks(){
        return totalSKS;
    }

}
