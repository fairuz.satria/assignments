package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI {

    private JComboBox[] acComboBoxs = new JComboBox[2];

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Tambah IRS
        Mahasiswa[] tempmMahasiswas = new Mahasiswa[SistemAkademikGUI.getDaftarMahasiswa().size()];
        MataKuliah[] tempKuliahs = new MataKuliah[SistemAkademikGUI.getDaftarMataKuliah().size()];
        SistemAkademikGUI.getDaftarMahasiswa().toArray(tempmMahasiswas);
        SistemAkademikGUI.getDaftarMataKuliah().toArray(tempKuliahs);
        acComboBoxs[0] = new JComboBox<>(tempmMahasiswas);                                                               // menambahkan object combo box bawaan swing kedalam array dengan parameter array yang diambil dari ArrayList daftarMahasiswa dan dantarMatakuliah
        acComboBoxs[1] = new JComboBox<>(tempKuliahs);
        
        Container container = frame.getContentPane();
        JLabel titLabel = new JLabel("Tambah IRS");
        titLabel.setFont(SistemAkademikGUI.fontTitle);
        titLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel[] aLabels = {new JLabel("Pilih NPM"), new JLabel("Pilih Nama Matkul")};
        
        JButton kembali = new JButton("Kembali");
        JButton tambahkan = new JButton("Tambahkan");
        tambahkan.setFont(SistemAkademikGUI.fontGeneral);
        tambahkan.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);

        kembali.addActionListener(new Actions(frame, "home"));
        tambahkan.addActionListener(new tambahIRS(frame));

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        JLabel[] space = new JLabel[11];
        for(int i = 0; i < 11; i++){
            space[i] = new JLabel(" ");
        }

        container.add(titLabel);
        container.add(space[0]);

        for(int i = 0; i < 2; i++){
            aLabels[i].setFont(SistemAkademikGUI.fontGeneral);
            aLabels[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            acComboBoxs[i].setMaximumSize(new Dimension(200, 24));
            acComboBoxs[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            container.add(aLabels[i]);
            container.add(space[(2*i)+1]);
            container.add(acComboBoxs[i]);
            container.add(space[(2*i)+2]);
        }

        container.add(tambahkan);
        container.add(space[5]);
        container.add(kembali);
    }


    // Uncomment method di bawah jika diperlukan
    MataKuliah getMataKuliah() {
        return (MataKuliah) acComboBoxs[1].getSelectedItem();                                                            // mereturn object yang terpilih oleh combo box dan men-downcast
    }

    Mahasiswa getMahasiswa() {
        return (Mahasiswa) acComboBoxs[0].getSelectedItem();
    }

    class tambahIRS implements ActionListener{

        JFrame frame;
    
        public tambahIRS(JFrame frame){
            this.frame = frame;
        }
    
        public void actionPerformed(ActionEvent e){
            if(getMahasiswa() == null || getMataKuliah() == null){
                JOptionPane.showMessageDialog(this.frame, "Mohon isi seluruh Field");
            }else{
                String temp = getMahasiswa().addMatkul(getMataKuliah());
                JOptionPane.showMessageDialog(this.frame, temp);
            }
        }
    }
}

