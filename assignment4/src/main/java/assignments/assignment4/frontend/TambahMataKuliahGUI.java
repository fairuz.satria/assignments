package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    private JTextField[] aJTextFields = new JTextField[4];

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        Container container = frame.getContentPane();
        JLabel titLabel = new JLabel("Tambah Mahasiswa");
        titLabel.setFont(SistemAkademikGUI.fontTitle);
        titLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        JLabel[] space = new JLabel[11];                                                                                 // membuat array jlabel ber-text " " untuk spacing
        for(int i = 0; i < 11; i++){
            space[i] = new JLabel(" ");
        }

        container.add(titLabel);
        container.add(space[0]);

        JLabel[] aLabels = {new JLabel("Kode Mata Kuliah:"), new JLabel("Nama Mata Kuliah:"), new JLabel("SKS:"), new JLabel("Kapasitas:")};
        for(int i = 0; i < 4; i++){
            aLabels[i].setFont(SistemAkademikGUI.fontGeneral);
            aLabels[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            aJTextFields[i] = new JTextField();
            aJTextFields[i].setMaximumSize(new Dimension(200, 24));
            aJTextFields[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            container.add(aLabels[i]);
            container.add(space[(2*i)+1]);
            container.add(aJTextFields[i]);
            container.add(space[(2*i)+2]);
        }

        JButton kembali = new JButton("Kembali");
        JButton tambahkan = new JButton("Tambahkan");
        tambahkan.setFont(SistemAkademikGUI.fontGeneral);
        tambahkan.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);

        kembali.addActionListener(new Actions(frame, "home"));
        tambahkan.addActionListener(new tambahmatkul(frame));

        container.add(tambahkan);
        container.add(space[9]);
        container.add(kembali);
    }

    private void setFieldEmpty(){
        for(JTextField x : aJTextFields){
            x.setText("");
        }
    }

    class tambahmatkul implements ActionListener{

        JFrame frame;
    
        public tambahmatkul(JFrame frame){
            this.frame = frame;
        }
    
        public void actionPerformed(ActionEvent e){
            if(aJTextFields[0].getText().equals("") || aJTextFields[1].getText().equals("") || aJTextFields[2].getText().equals("")||
            aJTextFields[3].getText().equals("")){
                JOptionPane.showMessageDialog(this.frame, "Mohon isi seluruh Field");
            }else{
                int sks = Integer.parseInt(aJTextFields[2].getText());
                int Kapasitas = Integer.parseInt(aJTextFields[3].getText());
                if(already()){
                    JOptionPane.showMessageDialog(this.frame, "Mata kuliah " + aJTextFields[1].getText() + " sudah pernah ditambahkan sebelumnya");
                    setFieldEmpty();                                                                                                                // mengosongkan seluruh field
                }else{
                    SistemAkademikGUI.addDaftarMatakuliah(new MataKuliah(aJTextFields[0].getText(), aJTextFields[1].getText(), sks, Kapasitas));
                    SistemAkademikGUI.sortmatkul(SistemAkademikGUI.getDaftarMataKuliah());
                    JOptionPane.showMessageDialog(this.frame, "Mata Kuliah " + aJTextFields[1].getText() + " berhasil ditambahkan");
                    setFieldEmpty();
                }
    
            }
        }
    
        private boolean already(){
           for(MataKuliah x : SistemAkademikGUI.getDaftarMataKuliah()){
               if(x.getNama().equals(aJTextFields[1].getText())){
                   return true;
               }
           }
           return false;
        }
    
    }
    
}
