package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa = {};

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
    }


    public void addMahasiswa(Mahasiswa mahasiswa) {                                             // method void menambah mahasiswa (setter)
        /* TODO: implementasikan kode Anda di sini */
        this.daftarMahasiswa = this.adds(this.daftarMahasiswa, mahasiswa);
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {                                            // metod void remove mahasiswa (setter)
        /* TODO: implementasikan kode Anda di sini */
        this.daftarMahasiswa = this.removes(this.daftarMahasiswa, mahasiswa);

    }

    public String getNama(){                                                                    //getter
        return this.nama;
    }

    public int getKapasitas(){
        return this.kapasitas;
    }

    public int getDafMahasiswas(){
        return this.daftarMahasiswa.length;
    }

    public Mahasiswa[] detMahasiswasArr(){
        return this.daftarMahasiswa;
    }

    public int getSks(){
        return this.sks;
    }

    public String getKode(){
        return this.kode;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return nama;
    }

    public Mahasiswa[] adds(Mahasiswa[] x, Mahasiswa x1){                                       // method menambahkan mahasiswa baru kedalam array
        Mahasiswa[] newa = new Mahasiswa[x.length + 1];                                         // membuat array dengan panjang array sebelumnya + 1
        for(int i = 0; i < x.length; i++){                                                      // mengassign array baru sama dengan array lama
            newa[i] = x[i];
        }
        newa[x.length] = x1;                                                                    // mengassign array index terakhir dengan mahasiswa baru
        return newa;
    }
    
    public Mahasiswa[] removes(Mahasiswa[] x, Mahasiswa x1){                                    // method remove mahasiswa yang diinginkan
        Mahasiswa[] newa = new Mahasiswa[x.length - 1];                                         // membuat array dengan panjang array sebelumnya + 1
        for(int i = 0, j = 0; i < x.length && j < newa.length; i++){                            // jika array dengan index sama dengan mahasiswa yang ingin di remove maka di continue
            if(x[i] == x1){
                continue;
            } else{                                                                             // jika tidak maka di assign
                newa[j] = x[i];
                j++;
            }
        }
        return newa;
    }
}
